function inputClick(clickedId) {
  console.log('clicked_id: ' + clickedId);

  _toggleClass(clickedId, 'active');

  if(_isClassExists('not-p', 'active')) {
    _removeClass('not-out', 'active');
  } else {
	_addClass('not-out', 'active');
  }

  _clearBackgroundsInTable();

  var idTable = '';
  idTable += _isClassExists('not-p', 'active') ? '1' : '0' ;
  idTable += _isClassExists('not-out', 'active') ? '1' : '0' ;
  _addClass(idTable, 'active');
}

function _clearBackgroundsInTable() {
  var el = document.getElementsByTagName('tr');
      
  for (var i =0; i < el.length; i++) {
    el[i].classList.remove('active');
  }
}

function _toggleClass(id, className) {
  document.getElementById(id).classList.toggle(className);
}

function _addClass(id, className) {
  document.getElementById(id).classList.add(className);
}

function _removeClass(id, className) {
  document.getElementById(id).classList.remove(className);
}

function _isClassExists(id, className) {
  return document.getElementById(id).classList.contains(className);
}